<?php               
error_reporting(E_ALL);
ini_set('display_errors', 1);                   
  require_once dirname(__FILE__).'/webservice/dbmysqli-mini.php';
  
  
  //modification du comportement des tables pour g�rer les mise a jour
  
  if (db_field_exists('ps_stock_available', 'upd')==false) {
      $sql='ALTER TABLE `ps_stock_available` ADD `upd` DATETIME NULL AFTER `out_of_stock`, ADD INDEX (`upd`);';
      db_command($sql);
      
      $sql="CREATE TRIGGER IF NOT EXISTS `ps_stock_avaible_before_insert` BEFORE INSERT ON `ps_stock_available` FOR EACH ROW set new.upd=now()";
      db_command($sql);
      
      $sql="CREATE TRIGGER IF NOT EXISTS  `ps_stock_avaible_before_update` BEFORE UPDATE ON `ps_stock_available` FOR EACH ROW if old.quantity<>new.quantity THEN 
    set new.upd=now();
    END IF"; 
    db_command($sql); 
   
    
      $sql="DELIMITER $$
                CREATE TRIGGER IF NOT EXISTS  `ps_product_shop_before_update` BEFORE UPDATE ON `ps_product_shop` FOR EACH ROW
                BEGIN
                if 
                    (old.available_for_order<>new.available_for_order) OR
                    (old.visibility<>new.visibility) OR
                    (old.on_sale<>new.on_sale) OR
                    (old.active<>new.active) THEN
                    SET new.date_upd=now();
                END IF;
                END$$

                DELIMITER ;
                "; 
    db_command($sql);       
        
  } 
   
  $db = new db_query();
  
  $fields="p.id_product, p.reference ,l.name as title,l.description_short as description,
s.quantity,
p.condition,
cl.name as category,
ps.available_for_order,
p.price,
concat('https://',(SELECT domain  FROM ps_shop_url where id_shop=1 and main=1),'/-/',p.id_product,'-',l.link_rewrite,'.html') as link,  
group_concat(concat('https://',(SELECT domain  FROM ps_shop_url where id_shop=1 and main=1),'/',if (i.id_image is null,'img/404.gif',concat(i.id_image,'-large_default/image-.jpg'))) order by i.position) as images_link,
if (fv.value is null or fv.value='','inconnu',fv.value) as editor,
if (fv2.value is null or fv2.value='','inconnu',fv2.value) as author,
p.ean13
";

if (isset($_REQUEST['fast'])) $fields="p.reference as id";

$last_where='';
if (isset($_REQUEST['last'])) $last_where=' and (p.date_upd>="'.$_REQUEST['last'].'" or (ps.date_upd>="'.$_REQUEST['last'].'" or s.upd>="'.$_REQUEST['last'].'")';
  
  
  $sql="SELECT $fields
FROM ps_product p 
    left join ps_product_shop ps on ps.id_product=p.id_product
    left join ps_product_lang l         on ps.id_product=l.id_product and ps.id_product = l.id_product
    left join ps_stock_available s    on ps.id_product=s.id_product and ps.id_shop=l.id_shop
    left join ps_image i                     on ps.id_product=i.id_product
    
    left join ps_category_product c on c.id_product=p.id_product
    left join ps_category_lang cl on c.id_category=cl.id_category and cl.id_shop=ps.id_shop and cl.id_lang=l.id_lang    
    
    left join ps_feature_lang fl on l.id_lang=fl.id_lang and fl.name='Editeur'
    left join ps_feature_product fp on fp.id_product=p.id_product and fp.id_feature=fl.id_feature
    left join ps_feature_value_lang fv on   fv.id_feature_value=fp.id_feature_value
    
    left join ps_feature_lang fl2 on l.id_lang=fl2.id_lang and fl2.name='Auteur'    
    left join ps_feature_product fp2 on fp2.id_product=p.id_product and fp2.id_feature=fl2.id_feature    
    left join ps_feature_value_lang fv2 on   fv2.id_feature_value=fp2.id_feature_value
    
    
    
where ps.id_shop=1 and l.id_lang=1 $last_where
group by p.id_product";   
$q=$db->query($sql);
$first=true;

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
$output = fopen("php://output", "w");
while($row=$db->fetch()){
    if ($first==true){
        fputcsv($output,array_keys($row));
        $first=false;
    }
    fputcsv($output, $row); 
    
}
fclose($output);         
  
?>
