<?php

    if (!defined('TIME_START')) define('TIME_START',microtime(true));
    require_once 'kint.phar';
    
    
    function get_backtrace($backtrace='') {
        if ($backtrace=='') $backtrace = debug_backtrace();
        $exc = array('exeptions.php', 'debug.php');
        $x = 0;
        $trace = "";
        while($x<sizeof($backtrace)) {
            if (isset($backtrace[$x]['file'])&& in_array(basename($backtrace[$x]['file']), $exc)) {
                $x++;
            } else break;
        }
        if (isset($backtrace[$x]['file'])) {
            $file = $backtrace[$x]['file'];
            $line = $backtrace[$x]['line'];
            $trace = array_slice($backtrace,$x, sizeof($backtrace)-$x);
        } else {
            $file = '';
            $line = '';
        }
        if (is_array($trace)) $trace = array_reverse($trace);

        return array($file, $line, $trace);
             
    } 
    
    function outputKint($data=null,$trace=null,$metainfo=true){
        global $_REQUEST,$_SESSION,$_SERVER,$_COOKIE;
        
        $stateReturn=Kint::$return;
        $enabled_mode=Kint::$enabled_mode;
        $cli_detection=Kint::$cli_detection;
        
        Kint::$return=true; //retourne valeur dans variable*/
        Kint::$enabled_mode = true;
        Kint::$cli_detection=false;  

        
        $return = 'Date:'.date("Y-m-d H:i:s").'<br>';
        $return .= 'temps:'.round(microtime(true)-TIME_START,4).'s<br>';
        if (isset($_SERVER['PWD'])) $return .= 'Chemin: '.$_SERVER['PWD']."<br>";
        if (isset($_SERVER['PHP_SELF'])) $return .= 'Script: '.$_SERVER['PHP_SELF']."<br>";


        $return .= Kint::dump($data);

       
       

        if ($metainfo==true) {
            $return .= Kint::dump($_SERVER,$_REQUEST,$_COOKIE,$_SESSION);
            if ($trace==null) $trace=get_backtrace(); 
            $return .= Kint::dump($trace);
           
        }

        
        Kint::$enabled_mode=$enabled_mode;
        Kint::$cli_detection=$cli_detection;        
        Kint::$return=$stateReturn; 

        return $return;
 

    } 
    function generateRandomString($length=10,$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }          
    
    function debugThat($data,$trace=null){
        $return=outputKint($data,$trace);
        $path=dirname(__FILE__).'/../log/';
        $file=$path.generateRandomString().'.html';
        file_put_contents($file,$return);
        if(rand(0,100)<5){
            filesCleaner($path.'/*.html',30);
            filesCleaner($path.'/*.log',365);
        }
    }
    function containG($container,$contained,$casse=true){
        if (is_array($container)) $container=serialize(($container));
        if ($casse==false) $container=mb_strtolower($container);
        if (!is_array($contained)) $contained=array($contained);
        foreach ($contained as $c){
            if ($casse==false) $c=mb_strtolower($c);
            if (strpos($container,$c)!==false) return true;    
        }
        
        return false;
    }    
function debug_output($type=null,$UsrType=null,$message='', $file='', $line='',$trace=null) { 
        global $_REQUEST,$_SESSION,$_SERVER;
        
        //debugOFF();
        $messagePlus='';
        if (is_array($message)) {
            $messagePlus=$message[1];
            $message=$message[0];
        }

        if (containG(array($file,$message),
                    array('Kint',
                          'kint',
                          "'tidy' already",  
                          'mysqli::close()',
                          'dbmysqli::__destruct()',
                          'STMT_CLOSE'))) return;

        if ($file=="Unknown") {
             return; //imap
        }          

        if ($UsrType=='debug') {
            $type_erreur = $type;
            if (empty($type)) $type = 0;        
            
        }  else {
            switch ($type) {
                case E_ERROR:
                case E_PARSE:
                case E_CORE_ERROR:
                case E_CORE_WARNING:
                case E_COMPILE_ERROR:
                case E_COMPILE_WARNING:
                case E_USER_ERROR:
                    $type_erreur = "Erreur fatale";
                    $type = 1;
                    break;

                case E_WARNING:
                case E_USER_WARNING:
                    $type_erreur = "Avertissement";
                    $type = 2;
                    break;

                case E_NOTICE:
                case E_USER_NOTICE:
                    $type_erreur = "Remarque";
                    $type = 3;
                    break;

                case E_STRICT:
                    $type_erreur = "Syntaxe Obsol?te";
                    $type = 4;
                    break;
                case 1001:
                    $type_erreur = "Erreur programmation";
                    $type = 4;
                    break;

                default:
                    $type_erreur = "Erreur inconnue";
                    $type = 5;
            }        
        }        
        
        if (is_string($message)&&empty($message)) return;
        
    

        
        if (empty($file)||empty($line)||empty($trace)){
        $a=get_backtrace();
            if (empty($file)) $file = $a[0];
            if (empty($line)) $line = $a[1];
            if (empty($trace)) $trace = $a[2];
        }
        $trace=debug_backtrace();
        
        $smal_report=array('type_erreur'=>$type_erreur,
                     'type'=>$type,
                     'message'=>$message,
                     'file'=>$file,
                     '$line'=>$line); 
                     
        echo $file.':'.$line."> ".$message;
        
        //var_dump($smal_report);       
        
        $report=array('type_erreur'=>$type_erreur,
                     'type'=>$type,
                     'message'=>$message,
                     'file'=>$file,
                     '$line'=>$line,
                     'trace'=>$trace);
        
        debugON();             
        debugThat($report,false);
        

        
    } 
    
    function debugON(){
        error_reporting(E_ALL & ~E_WARNING);
        @ini_set('display_errors', 1);
        set_error_handler('debugGerr',E_ALL);
        set_exception_handler("debugGerrExep");
        register_shutdown_function('debugGerrFatal'); 
    }
    function debugOFF(){
        error_reporting(0);
        @ini_set('display_errors', 0);    
        set_error_handler('ErrorVoid',E_ALL);
        set_exception_handler("ErrorVoid");
        register_shutdown_function('ErrorVoid'); 
               
    }  
    
function debugGerr($type=null, $message=null, $file=null, $line=null){
    global $debugG; 
    $userType='';      
    debug_output($type, $userType, $message, $file, $line,null); 
}
function debugGerrExep($exception){
    global $debugG;    
    $message=$exception->getMessage();
    $file=$exception->getFile();
    $userType=$exception->getCode();
    $line=$exception->getLine();
    $trace=$exception->getTrace();
    $type=E_USER_ERROR; 
    debug_output($type, $userType, $message, $file, $line,$trace);
  
}
function debugGerrFatal(){
    global $debugG;    
    $e=error_get_last();
    if ($e==null) exit;
    $type = isset($e['type']) ? $e['type'] : 0;
    $message = isset($e['message']) ? $e['message'] : '';
    $file = isset($e['file']) ? $e['file'] : '';
    $line = isset($e['line']) ? $e['line'] : '';
    $userType=''; 
    debug_output($type, $userType, $message, $file, $line,null);

}  
function filesCleaner($path='tmp/*.html',$days=1){
    
    $files = glob($path);
    $time   = strtotime('-'.$days.' days');

    foreach ($files as $file) {
        if (is_file($file)) {
          if ($days==0 || $time > filemtime($file)) { // 2 days
            @unlink($file);
          }        
        } 
    }   
    
}       
    
        
?>
