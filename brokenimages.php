<?php
/*prepare list of broken images*/
require(dirname(__FILE__).'/config/config.inc.php'); //  you might need to change the path to the file 'config.inc.php' if you are not in the root folder
$id_lang = 1; //getting links for one language. This is enough to identify empty images
$i = 0;
$limit = 100;
$filename = '/tmp/broken_urls.txt';
file_put_contents($filename, '', LOCK_EX); //remove old content
//file_put_contents($filename, '', FILE_APPEND | LOCK_EX); //append content
do {
    $images = Db::getInstance()->executeS('
        select *
        from `'._DB_PREFIX_.'image` i
        limit ' . (int)($i++ * $limit) . ', ' . (int)$limit . '
    '); //receiving the stack of images

    foreach ($images as $image) {
        $imgObj = new Image($image['id_image'], $id_lang);
        $image_path = _PS_PROD_IMG_DIR_ . $imgObj->getImgPath() . '.' . $imgObj->image_format;
        if (!file_exists($image_path)) { //verifying, whether the file with the image exists
            $productObj = new Product($image['id_product'], NULL, $id_lang); //creating a product object for link generation

            #print_r($image['id_product'] . ' => ' . Context::getContext()->link->getImageLink($productObj->link_rewrite, $image['id_image']) . chr(10)); //die;
            print_r($image['id_product'] . ','); //die;
            //file_put_contents($filename, $image['id_product'] . ' => ' . Context::getContext()->link->getImageLink($productObj->link_rewrite, $image['id_image']) . chr(10));
        }
    }
} while(count($images));
echo '0';
