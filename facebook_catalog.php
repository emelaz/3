<?php               
error_reporting(E_ALL);
ini_set('display_errors', 1);                   
  require_once dirname(__FILE__).'/webservice/dbmysqli-mini.php';
  $db = new db_query();
  
  $fields="p.reference as id,l.name as title,l.description_short as description,
if (p.available_for_order=1 and s.quantity>0,'in stock','out of stock') as availability,
s.quantity as inventory,
if (p.condition='new','new','used') as `condition`,
concat(p.price,' ',(SELECT iso_code  FROM ps_currency WHERE active=1)) as price,
concat('https://',(SELECT domain  FROM ps_shop_url where id_shop=1 and main=1),'/-/',p.id_product,'-',l.link_rewrite,'.html') as link,  
concat('https://',(SELECT domain  FROM ps_shop_url where id_shop=1 and main=1),'/',if (i.id_image is null,'img/404.gif',concat(i.id_image,'-large_default/image-.jpg'))) as image_link,
if (fv.value is null or fv.value='','inconnu',fv.value) as brand";

if (isset($_REQUEST['fast'])) $fields="p.reference as id";
  
  
  $sql="SELECT $fields
FROM ps_product p 
    left join ps_product_lang l on p.id_product=l.id_product 
    left join ps_stock_available s on s.id_product=p.id_product and s.id_shop=l.id_shop
    left join ps_image i on i.id_product=p.id_product and position=1
    left join ps_feature_lang fl on l.id_lang=fl.id_lang and fl.name='Editeur'
    left join ps_feature_product fp on fp.id_product=p.id_product and fp.id_feature=fl.id_feature
    left join ps_feature_value_lang fv on   fv.id_feature_value=fp.id_feature_value
where l.id_shop=1 and l.id_lang=1";   
$q=$db->query($sql);
$first=true;

header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
$output = fopen("php://output", "w");
while($row=$db->fetch()){
    if ($first==true){
        fputcsv($output,array_keys($row));
        $first=false;
    }
    fputcsv($output, $row); 
    
}
fclose($output);         
  
?>
