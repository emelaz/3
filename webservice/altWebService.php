<?php
if (!class_exists('altWebService')){
    class altWebService{
        var $content;

        var $method;
        var $url;
        var $params;         
        var $fx;
        var $input_xml;

        function __construct($method, $url, $params, $input_xml){
            $this->method=$method;
            $this->url=$url;
            $this->params=$params;
            
            $this->input_xml=$input_xml;
            
            $fx=$params['fx'];
            $this->$fx();
            
        }
        function output(){  
            return array(
                        'headers'=>array(),
                        'type'=>'json',
                        'content'=>json_encode($this->content)
            );    
        }
        function newsLetterRecipients(){
            $sql="SELECT concat(firstname,' ',lastname) as fullname, email FROM `ps_customer` WHERE newsletter_date_add>'".$this->params['limit']."'"; $this->content=db_getList($sql);           
        }
        /**
        * Produits sans cat�gorie qui sont donc fantomes
        * 
        */
        function orphans_products(){
            $limit= isset($this->params['limit']) ? $this->params['limit'] : 100000;
            $start= isset($this->params['start']) ? $this->params['start'] : 0;
            $sql='SELECT a.id_product from ps_product a left join ps_category_product b on a.id_product=b.id_product where b.id_category=0 or b.id_category is null LIMIT '.$this->params['limit'];   
            $this->content=array_column(db_getList($sql),'id_product');   
        }
        function products(){
            $select='id_product as id, id_category_default as cat, reference as sku, date_upd as at, minimal_quantity as qty, price';
            if (isset($this->params['display'])) $select=$this->params['display'];
            $sql='SELECT '.$select.'
                  FROM ps_product 
                  ORDER BY id_product 
                  LIMIT '.$this->params['limit'];
            $this->content=db_getList($sql);
        }
        function getPaymentID(){
            $sql='SELECT id_order_payment FROM `ps_order_invoice_payment`  where id_order = '.$this->params['display'];
            $this->content=array('id'=>db_getString($sql));
        }
        function productHide(){
            $id =0;
            if (isset($this->params['display'])) $id=(int)$this->params['display'];
            $sql='UPDATE `ps_product_shop` SET `visibility` = "none", available_for_order="0" 
                      WHERE `ps_product_shop`.`id_product` = '.(int)$id.' 
                      AND `ps_product_shop`.`id_shop` = 1;';
            $r=db_command($sql);
            $this->content=array('ack'=>true,'r'=>$r);
        }
        function productShow(){
            $id =0;
            if (isset($this->params['display'])) $id=(int)$this->params['display'];
            $sql='UPDATE `ps_product_shop` SET `visibility` = "both", available_for_order="1" 
                      WHERE `ps_product_shop`.`id_product` = '.(int)$id.' 
                      AND `ps_product_shop`.`id_shop` = 1;';
            $r=db_command($sql);
            $this->content=array('ack'=>true,'r'=>$r);
        }
        function productsCount(){
            $sql='SELECT count(*) FROM ps_product ';
            $this->content=array('count'=>db_getString($sql));
        }
        function productGetRemoteID(){
            $sql='SELECT id_product FROM ps_product where reference="'.$this->params['display'].'"';
            $this->content=array('ack'=>true,'id'=>db_getString($sql));
        }
        function ClearHomeCache(){
            $sql="delete  FROM `ps_smarty_cache` WHERE `cache_id` LIKE '%blocknewproducts%' or `cache_id` LIKE '%home%' or `cache_id` LIKE '%categories%'";
            $r=db_command($sql);
            $this->content=array('ack'=>true,'r'=>$r);
        }
        function categories(){
            db_command('delete from ps_category_group where id_category in (select * from (SELECT a.id_category FROM `ps_category_group` a left join ps_category b on a.id_category=b.id_category where b.id_category is null) as tbl)');
            $root=db_getString('select id_category from ps_category where is_root_category=1');
            if($root=='')$root='2';
            $sql='select id_category as id,id_parent as parent from ps_category where is_root_category=0 and id_category>1';
            $list=db_getList($sql);
            $this->content=array('root'=>$root,'list'=>$list);
            
        }
        function features(){
            $sql='SELECT id_feature as id, name FROM `ps_feature_lang` where id_lang=1';
            $list=db_getList($sql,true,'id','name');
            $this->content=$list; 
            
        }
        function productGetFeatures(){
            $indexes=array_keys(db_getIndex('ps_feature_value_lang'));
            if (!in_array('value',$indexes)){
                db_command('ALTER TABLE `ps_feature_value_lang` ADD INDEX(`value`);');
            }
            $indexes=array_keys(db_getIndex('ps_product'));
            if (!in_array('reference',$indexes)){
                db_command('ALTER TABLE `ps_product` CHANGE `reference` `reference` VARCHAR(20) NOT NULL;');
                db_command('ALTER TABLE `ps_product` ADD UNIQUE(`reference`);');
            }
            $in=json_decode($this->params['display'],1);
            $return=array();
            $id_product=null;
            $infos=null;
            if ( $infos=db_get('select a.id_product, b.link_rewrite, c.quantity, group_concat(id_image) as images 
                               from ps_product a
                                    left join ps_product_lang b 
                                        on a.id_product=b.id_product and
                                           b.id_shop=1 and 
                                           id_lang=1 
                                    left join ps_stock_available c 
                                        on a.id_product=c.id_product and
                                           c.id_shop=1
                                    left join ps_image d 
                                        on a.id_product=d.id_product
                               where reference="'.$in['reference'].'"
                               /*group by a.id_product*/')) 
            {
                $id_product=$infos['id_product']; 
                if (!empty($infos['images'])) {
                    $infos['images']=explode(',',$infos['images']);
                    $id_lang=1;
                    $checksumArray=array();
                    foreach ($infos['images'] as $id_image) {
                        $imgObj = new Image($id_image, $id_lang);
                        $image_path = _PS_PROD_IMG_DIR_ . $imgObj->getImgPath() . '.' . $imgObj->image_format;
                        $fs=_PS_PROD_IMG_DIR_ . $imgObj->getImgPath() . '.' . $imgObj->image_format;
                        $checksumArray[$id_image]=file_exists($fs)?sha1_file($fs):'';
                        /*
                        if (!file_exists($image_path)) { //verifying, whether the file with the image exists
                            $productObj = new Product($id_image, NULL, $id_lang); //creating a product object for link generation
                            $infos['images_more'][$id_image]=Context::getContext()->link->getImageLink($productObj->link_rewrite, $id_image);
                        }*/
                    } 
                    $infos['images_checksum']=sha1(serialize($checksumArray));                   
                    
                }                     
            }


            
            foreach($in['feature'] as $id=>$val) {
                $sql='SELECT a.id_feature_value 
                      FROM ps_feature_value_lang a 
                      LEFT JOIN ps_feature_value b on a.id_feature_value=b.id_feature_value
                      WHERE value = "'.addslashes($val).'" and 
                            b.id_feature= '.$id. ' and
                            id_lang=1';
                $r = db_getString($sql);
                if ($r==''){
                    $ar=array('id_feature'=>$id);
                    $q=db_perform('ps_feature_value',$ar);
                    $r=$q->id;
                    $ar=array('id_lang'=>'1','value'=>$val,'id_feature_value'=>$r);
                    $q=db_perform('ps_feature_value_lang',$ar);
                } 
                $return[$id]=$r;   
            }
            
            $this->content=array('feature'=>$return,'id_product'=>$id_product, 'infos'=>$infos); 
            
            
        }
        function newProduct() {
            /*INSERT INTO `ps_product` (`id_product`, `id_supplier`, `id_manufacturer`, `id_category_default`, `id_shop_default`, `id_tax_rules_group`, `on_sale`, `online_only`, `ean13`, `upc`, `ecotax`, `quantity`, `minimal_quantity`, `price`, `wholesale_price`, `unity`, `unit_price_ratio`, `additional_shipping_cost`, `reference`, `supplier_reference`, `location`, `width`, `height`, `depth`, `weight`, `out_of_stock`, `quantity_discount`, `customizable`, `uploadable_files`, `text_fields`, `active`, `redirect_type`, `id_product_redirected`, `available_for_order`, `available_date`, `condition`, `show_price`, `indexed`, `visibility`, `cache_is_pack`, `cache_has_attachments`, `is_virtual`, `cache_default_attribute`, `date_add`, `date_upd`, `advanced_stock_management`, `pack_stock_type`) VALUES ('0', '0', '0', '123', '1', '0', '0', '0', '', '', '0.000000', '0', '1', '150.000000', '0.000000', '', '0.000000', '0.00', '29424055', '', '', '0.000000', '0.000000', '0.000000', '0.000000', '2', '0', '0', '0', '0', '1', '', '0', '1', '0000-00-00', 'good', '1', '1', 'both', '0', '0', '0', '0', '2019-06-20 10:36:26', '2019-06-20 10:36:26', '0', '0');*/
            
            
        }
        function categoriesUpdate(){
            $sql="update `ps_category` set active='0' where id_category=(SELECT id_category  FROM `ps_category_lang` WHERE `name` LIKE '\\%\\%trash\\%\\%')";
            db_command($sql);
            Category::regenerateEntireNtree(true);   
            $this->content=array('ack'=>true);         
        }
    }
}  
?>
