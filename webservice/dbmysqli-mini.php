<?php    
if (!class_exists('db_query')){
$currentDir = dirname(__FILE__);
require_once($currentDir.'/../config/settings.inc.php');
  class db_query{
      var $db;
      var $dbName;
      var $result;
      var $rows;
      var $id; //alias de insert_id
      var $insert_id;
      var $supressErrors=false;
      var $sql;
      var $errors=array();
      var $log=false; //mettre un chemin pour logger
      //var $log='/home/mysql/logs/';
      var $logExcludeType=array('select','show');
      //var $logExcludeTbl=array(GLIVRES_DB_DATABASE,GLIVRES_CRON_DATABASE);
      var $time='';
      var $verbose_time=false;
      
      var $inTransaction=false;
      var $rollback=false;
      var $count_transaction=0;
      var $max_transaction = 10000;
      var $locked=null;
      var $charset = "utf8mb4";
      
      var $deadLockRetry=0;
      var $deadLockMaxRetries=3;
      var $deadLockWait=100000;
      
      
      function __construct($sql=null, $dbName=null, $user=null, $pwd=null, $host='localhost'){
        $this->connect($user, $pwd, $dbName,$host);
        if (!empty($this->log)) mkd($this->log);
        
        if (!empty($sql)) $this->query($sql);
        if (defined('DBsupressErrors')) $this->DBsupressErrors;
      }
      function connect($user, $pwd, $dbName,$host){
        if (empty($user) && defined('_DB_USER_')) $user=_DB_USER_;  
        if (empty($pwd)&& defined('_DB_PASSWD_')) $pwd=_DB_PASSWD_;  
        if (empty($dbName)&& defined('_DB_NAME_')) $dbName=_DB_NAME_;
        if (empty($host)&& defined('_DB_SERVER_')) $host=_DB_SERVER_;

        if (substr($dbName,-1)=='.')$dbName=substr($dbName,0,strlen($dbName)-1);
        $this->db = new mysqli($host, $user, $pwd, $dbName);
        $this->dbName=$dbName;
        if($this->db->connect_errno > 0){
            $this->throwException('Unable to connect to database [' . $this->db->connect_error . ']',__LINE__);
        }
        
        $this->db->set_charset($this->charset);        
      }
      function begin_transaction($locked=null) {
          if (!empty($locked)) {
              $tableLock=$locked;
              if (!is_array($tableLock)) $tableLock=array($tableLock);
              foreach ($tableLock as &$value) {
                    $value .= ' read';
              }
              unset($value);              
              
              $this->query('lock tables '.implode(',',(array)$tableLock));
              $this->locked=$locked;
              //d('lock tables '.implode(',',(array)$tableLock).' read');
          } else {
              $this->locked=null;
          }
            $this->inTransaction=true;
            $this->db->autocommit(false);
            return $this->db->begin_transaction(); 
          
      }
      public function end_transaction($close=false) {  
            //if ($this->result) $this->result->free();
            if ($this->rollback == false) {
                $this->db->commit();
                $this->count_transaction=0;
            } else {
                $this->db->rollback();
                $this->rollback = false;
            }
            $this->db->autocommit(true);
            $this->inTransaction = false;
            if ($close==true) {
                
                $this->db->close();     
            }
            //ddd($this,$this->db);
            if (!empty($this->locked)) $this->query('unlock tables');
      }                  
      function query($sql, $useResult=false){ 
          
        /*if (contain($sql,' orders ') && contain($sql,'delete ')) {
            dt($sql);
        }*/    
          
        //d(memory_get_usage(),$sql);  
        if (substr($sql,0,9)=='noerror: ') {
            $this->supressErrors=true;
            $sql=str_replace('noerror: ','',$sql);
        }  
        $this->sql=$sql;
        $opt=$useResult==true?MYSQLI_USE_RESULT:null;
        
        $transacReturn=false;
        $start=false;
        if ($this->inTransaction == true) {
            $this->count_transaction++;
            if ($this->count_transaction>=$this->max_transaction){
                $this->end_transaction();
                $transacReturn=true;
                $this->begin_transaction($this->locked);
            }          
        } else {
            $start=microtime(true);
        }       
         
        if(!$this->result = $this->db->query($sql,$opt)){
            
            $errno=$this->db->errno;
            $this->errors[]=$errno;
            if ( in_array($errno,array(1205,1213)) && $this->inTransaction==false) {  
                $this->deadLockRetry++;
                if ($this->deadLockMaxRetries>$this->deadLockRetry) {
                    usleep($this->deadLockWait);
                    //dt(array('deadLOCK'=>$sql));          
                    return $this->query($sql, $useResult);                    
                }

                
            }
            $this->throwException('There was an error ['.$errno. ':'. $this->db->error.'] running the query [' . $sql. ']',__LINE__);
        } else
            $this->deadLockRetry=0;
        
        
        if ($start!=false) $this->time=microtime(true)-$start;
        else $this->time='';
        
        if ($this->verbose_time==true&&$this->time!='') echo "\n<br>".round($this->time,6). ": $sql\n<br>";
        
        $this->logQuery();
        
        $this->rows();
        if ($this->inTransaction == true) return $transacReturn;
        
        return $this->result;              
      }
      function logQuery(){
          if (empty($this->log)) return;
          if (!empty($this->logExcludeTbl)&&in_array($this->dbName,$this->logExcludeTbl)) return;
          exec('chmod 777 '.$this->log);
          
          $l=strtr($this->sql,array(chr(10)=>' ',chr(13)=>' ',chr(9)=>' '));
          $l = trim(preg_replace('/\s+/', ' ',$l));
          $type=strtolower(explode(' ',$l)[0]);          
          if (!empty($this->logExcludeType)&&in_array($type,$this->logExcludeType)) return;
          
          error_log(date('Y-m-d').chr(9).date('H:i:s').chr(9).$this->time.chr(9).$this->dbName.chr(9).$l.chr(10),3,$this->log.date('Y-m-d').'.log'); 
          
      }
      function fetch(){          
        $r=$this->result->fetch_assoc();
        if (!$r) {
            $this->result->free();
        }
        return $r;
      }
      function rows(){
          if(!$this->result) return false;
          if (isset($this->db->num_rows)) $this->rows=$this->db->num_rows;
          elseif (isset($this->db->affected_rows)) $this->rows=$this->db->affected_rows;
          if (isset($this->db->insert_id)) {
              $this->id=$this->db->insert_id;
              $this->insert_id=$this->db->insert_id;
          }
          else return false;
          
          
          return true;
      }
      function getString($idx=0){
        $count=$this->rows;
        if ($count != 1) return false;
        $r=$this->fetch();
        $values=array_values($r);
        return $values[$idx];
      }
      function get($noResultReturn=false){
        $count=$this->rows;
        if ($count != 1) return $noResultReturn;
        return $this->fetch();
      }
      function getList($sql,$key=null,$val=null){
        $this->query($sql);
        $return=array();
        while ($row=$this->fetch()){
            if (!empty($key)) $k=$row[$key];
            if (!empty($val)) $row=$row[$val];
            if (!empty($key)) $return[$k] = $row; else $return[] = $row;  
        }
        if (1==2) return SplFixedArray::fromArray($return);
        else return $return;      
      }                  
      function __destruct(){
          try{
            if ($this->db) $this->db->close();    
          } catch (Exception $e) {              
          }          
      }
      protected function throwException($msg='', $line=0) {
            if ($this->supressErrors==false) {
                throw new Exception ($msg);
            }
            return false;
      }          
      
  }
}
if (!class_exists('db_perform')){
  class db_perform extends db_query{
      function __construct($table=null, $data=null, $action='insert', $parameters="", $db=null, $user=null, $pwd=null, $host='localhost'){
        $this->connect($user, $pwd, $db,$host);
        if (!empty($table)) {
            return $this->perform($table,$data,$action,$parameters);
        }             
      }
      
      function perform($table,$data,$action='',$parameters=''){
          $sql=$this->arSQL($table,$data,$action,$parameters);
          return $this->query($sql);
      }

      function arSQL($table,$data,$action='',$parameters='') {
          
        if ($action == 'insert'||empty($action)) {
            $sql = $this->iDBConstruct_insert($table, $data);
        } elseif ($action == 'update') {
            $sql = 'update ' . $table . ' set ';
            $sql .= $this->iDBConstruct_update($data);
            $sql .= ' where ' . $parameters;
        } elseif ($action == 'updateinsert' || $action == "insertupdate") {
            if ($this->inTransaction === true) {
                $this->throwException($sql . ': Cannot do Update/Insert when preparing transactions', __LINE__);
            }  
            $sql = "select * from $table where " . $parameters;

            $this->query($sql);
            $num = $this->getString(0);             
            
            //$num = $this->countRows($sql);
            if ($num==0) return $this->arSQL($table, $data, 'insert');
            else return $this->arSQL($table, $data, 'update', $parameters);

        } elseif ($action == 'key' ) {
            $sql = $this->iDBConstruct_insert($table, $data);
            $sql .= ' ON DUPLICATE KEY UPDATE ';
            $sql .= $this->iDBConstruct_update($data);
        }  elseif ($action == 'ignore' ) {
            $sql = $this->iDBConstruct_insert($table, $data, 'ignore');
        } else {

            $this->throwException($sql . ": Method '$action' not supported for iDBConstruct function", __LINE__);    
        }
        //d($query);
        return $sql;
    }
    
        private function iDBConstruct_insert($table, $data, $option='') {
            $sql = '';
            $keys=array_keys($data);
            for($x=0;$x<sizeof($keys);$x++){
                if (strstr($keys[$x],'`')==false) $keys[$x]='`'.$keys[$x].'`';
            }
            $sql = 'insert '.($option!='' ? trim($option).' ':'').'into ' . $table . ' (' . implode(', ', $keys) . ') values (';
            foreach(array_values($data) as $value) {
                $sql .= $this->iDBConstruct_clean_data($value). ', ';   
            }
            $sql = substr($sql, 0, -2) . ')';
                
            return $sql;        
        }
        private function iDBConstruct_update($data) {
            $sql = '';
            foreach($data as $columns=>$value) {
                if (strstr($columns,'`')==false) $columns='`'.$columns.'`'; 
                $sql .= $columns . ' = '.$this->iDBConstruct_clean_data($value). ', ';
            }
            $sql = substr($sql, 0, -2);
            return $sql;        
        }
        private function iDBConstruct_clean_data($value) {
            switch ((string)$value) {
                case 'now()':
                    $return = 'now()';
                    break;
                case 'null':
                    $return = 'null';
                    break;
                default:
                    $return = '\'' . addslashes($value) . '\'';
                    break;
            }
            return $return;        
        }                  

  }
}
if (!class_exists('db_structure')){
  class db_structure extends db_query{
      
      function __construct($db=null, $user=null, $pwd=null, $host='localhost'){
        $this->connect($user, $pwd, $db,$host);
      }      
/**
* Existence d'une base de donnÃ©: retourne true ou false*     
* @param mixed $database
*/
    function database_exist($database = _DB_NAME_) {
        
        $r = $this->getList("SHOW DATABASES");
        $databases = array();
        foreach ($r as $t) {
            $databases[] = $t["Database"]; 
        }
        if (in_array($database, $databases)) return true;
        else return false;

    }
/**
* Existence d'une table ( option: dans une base de donnÃ©)
* retourne true ou false
* 
* @param mixed $table string ou array liste de nom de tables
* @param mixed $database
* @found array en retour tables trouvÃ©e
* @notFound array en retour tables non trouvÃ©e
*/
    function table_exist($tables, $database = null, &$found=null, &$notFound=null) {
        if ($database==null) $database=$this->select_db();
        
        if (!is_array($tables)) $tables=array($tables);
        $found = array_column($this->getList("SHOW TABLES FROM `$database` where Tables_in_$database in ('".implode("','",$tables)."')"),"Tables_in_$database");
        
        $notFound=array_diff($tables,$found);
        
        if (sizeof($found)==sizeof($tables)) return true;
        return false;
        
    }
    function tables_inUse($database=null){
        $sql="SHOW OPEN TABLES WHERE In_use > 0" . (!empty($database) ? ' and `Database`="'.$database.'"' : '');
        return $this->getList($sql);
    }
/**
* Existence d'un champs dans une une table ( option: dans une base de donnÃ©)
* retourne true ou false
* @param mixed $table
* @param mixed $field
* @param mixed $db
*/
    function field_exists($table, $field, $db="") {
        $fields = $this->tableStructure($table, $db);
        for ($x=0; $x<sizeof($fields); $x++) {
            if (strtolower($fields[$x]['Field'])==strtolower($field)) return true;    
        }
        return false;
    }
    function getPartitions($table,$database=null) {

        if ($database==null) $database=$this->dbName;
        $return=$this->getList("SELECT * FROM information_schema.partitions WHERE TABLE_SCHEMA='".$database."' AND TABLE_NAME = '".$table."' AND PARTITION_NAME IS NOT NULL","PARTITION_NAME");
        if (sizeof($return)==0) return null;
        else return $return;
    }     
/**
* Retourne la liste des tables
*        
* @param mixed $database (optionnel)
*/
    function getTables($database=null) {
        if ($database==null) $database=$this->databaseName;        
        return array_column($this->getList("SHOW TABLES FROM `$database` "),"Tables_in_$database");      
    }
 /**
* Renvoi la liste des champs d'une table
*     
* @param mixed $table
* @param mixed $db
*/
    function getFields($table, $db="") {
        $fields = $this->tableStructure($table, $db);
        return arrayFlat($fields);    
    }
/**
* Renvoi la liste des champs avec leur caractÃ©risttique ou juste le champ prÃ©cisÃ©
*     
* @param mixed $table
* @param mixed $db
* @param mixed $field
*/
    function getFieldsStructure($table, $db="", $field="") {
        $fields = $this->tableStructure($table, $db);
        $return=array();
        foreach ($fields as $f) {
            $k=$f["Field"];
            $v=$f;
            unset($v["Field"]);
            $return[$k]=$v;    
        }
        if ($field=="") return $return;
        else return $return[$field];   
    }
    function getPrimaryKey($table,$db='DATABASE()'){
        $sql="SELECT k.COLUMN_NAME FROM information_schema.table_constraints t LEFT JOIN information_schema.key_column_usage k USING(constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY' AND t.table_schema=$db AND t.table_name='$table'";
        return $this->getString($sql);
    }
    function enumGet($table, $field, $db=""){
        $structure=$this->getFieldsStructure($table,$db,$field);
        if (preg_match_all('/\'(.+?)\'/m', str_replace("''",'[apo]',$structure['Type']), $matches, PREG_SET_ORDER, 0)) {
            $return=array_column($matches,1);
            foreach ($return as $key=>$value) {
                $value=str_replace('[apo]',"'",$value);
                $return[$key] = stripslashes($value);
            } 
            return $return;           
        }
            
        return false;    
    }
    function enumPut($table, $field, $values, $options="CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL", $db=""){
        if ($db!='') $db='`'.$db.'`.';
        $values=(array)$values;
        if (sizeof($values)>256) {
            throw new Exception ('trop de ligne champs enum', __FILE__, __LINE__, debug_backtrace(),99);
        }
        foreach ($values as $key=>$value) {
            $values[$key] = addslashes($value);
        } 
        $sql="ALTER TABLE ".$db."`".$table."` CHANGE `".$field."` `".$field."` ENUM('".implode("','",array_iunique($values))."') ".$options;
        $this->query($sql);
    }
/** vÃ©rifie que le $field de la $table contient toutes les $values ou update
* put your comment there...
*      
* @param mixed $table
* @param mixed $field
* @param string ou array $values
* @param mixed $db optionnel
*/
    function enumCheck($table, $field, $values, $db=""){
        $enum=$this->enumGet($table,$field,$db);
        if (!is_array($enum)) $enum=array();
        if (!is_array($values)) $values=array($values);
        $ack=false;
        foreach($values as $val) {
            $val=trim($val);
            if (!in_array($val,$enum)) {
                $options= $this->getFieldsStructure($table,$db)[$field]['Null'] == 'NO' ? 'NOT NULL': 'NULL';
                $enum[]=$val;
                $this->enumPut($table, $field, $enum, $options, $db);
                $ack=true;
            }
        }
        return $ack;
        
    }

    private function tableStructure($table, $db="") {
        $return = array();
        if ($db != "") $db = $db."."; 
        $cols = $this->getList("SHOW COLUMNS FROM $db$table");
        return $cols;    
    }
    
/**
* Fait un "insert into tbl (select ***)
* 
* Ex: select into orders_stats_1 select (orders_id, year(orders_date), count(*))
* devient mapper('orders_stats_1',
*                array('orders_id'=>'orders_id','year'=>'year(orders_date)'),'compte'=>count(*)),
*                'from orders')
* 
* @param mixed $insertOrTable: La table ou insÃ©rer ou le dÃ©but de la requette
*                            Ex: orders_stats_1 
*                            Ex insert ignore into 
* @param mixed $dataMap: Le contenu du select
*                        un array de pair (champDest=>champSource).
*                        L'ordre des pairs ne compte pas
* @param mixed $fromWhere le reste de la requette de select
* @param string $order pour eviter les deadlock, orderBY 
* @param bolean $launch lance la requette (true) ou la retourne (false)
* 
*/
    function mapper($insertOrTable,$dataMap,$fromWhere='',$order=null,$launch=true){
        
        $insertOrTable=strtolower($insertOrTable);
        if (sizeof(explode(' ',$insertOrTable))==1) {
            $table=$insertOrTable;
            $insertOrTable='insert into '.$insertOrTable;
            
        } else {
            $table=explode('into',$insertOrTable)[1];
        }
        
        $def=$this->getFieldsStructure($table);
        
        $clean=array();
        foreach ($dataMap as $k=>$v){//map avecle même nom de champs
        //ddd(is_integer($k),$v,in_array($v,array_keys($def)),$def);
            if (is_integer($k) && in_array($v,array_keys($def))) {
                $clean[$v]=$v;
            } else {
                //if (contain($v,array(',','"'))) $v=addslashes($v);
                $clean[$k]=$v;
            }
        }
        $dataMap=$clean;
        
        foreach($def as $field=>$content){
            $val=($content['Default']==null)?'null':'"'.$content['Default'].'"';
   
            if($field!='null'&&isset($dataMap[$field])) {
                $val=$dataMap[$field];
                unset($dataMap[$field]);  
            }
            $feedArray[]=$val;   
        }
        
        if (!empty($order)) $order=' order by '.$order;
        $feedString='(select '.implode(",\n",$feedArray).' '.$fromWhere. $order.')';
        $feed=$insertOrTable.' '.$feedString;
        /*
        if (!contain($feedString,$table)) $feed=$insertOrTable.' '.$feedString;
        else $feed=$insertOrTable.' select * from ('.$feedString.' as trickTbl)'; */
        //d($feed);
        if (sizeof($dataMap)>0) throw new Exception('Certains mappages n\'ont pas de correspondaces dans la table cible: '.implode(',',array_values($dataMap)),__FILE__,__LINE__,null,99);
        if ($launch==true) return $this->query($feed);
        else return $feed;    
    }    
           
  }
}


if (!function_exists('db_getString')) {
  function db_query($sql,$database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_query(null,$database,$user,$pwd,$host);
    $db->query($sql);
    return $db;
  }    
  function db_getString($sql,$idx=0,$db=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_query($sql,$db,$user,$pwd,$host);
    return $db->getString($idx);
  }
  function db_get($sql,$db=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_query($sql,$db,$user,$pwd,$host);
    return $db->get();      
  }
  function db_getList($sql,$fast=true,$key=null,$val=null,$database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_query(null,$database,$user,$pwd,$host);
    return $db->getList($sql,$key,$val);
  }  
  function db_command($sql,$database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_query($sql,$database,$user,$pwd,$host);
    return $db->rows;
  }    
/**
* Compte le nombre de lignes d'une requette
*   
* @param mixed $sql de preference, faire select count(id) from table ou la fonction se dÃ©brouille seule
* @param mixed $database
* @param mixed $user
* @param mixed $pwd
* @param mixed $host
*/
  function db_count($sql,$database=null, $user=null, $pwd=null, $host='localhost'){
      
      /*
      if (contain(strtolower($sql),'select count(')==false) {
          require_once(dirname(__FILE__).'/PHP-SQL-Parser/src/PHPSQLParser.php');
          require_once(dirname(__FILE__).'/PHP-SQL-Parser/src/PHPSQLCreator.php');
          $parser=new PHPSQLParser($sql);
          $parse=$parser->parsed;
          $key=$parse['SELECT'][0];
        
          $alias='c';
          if ($key['base_expr']=='count') {
            if ($key['alias']==false) $key['alias']=$alias;
          } else {
            $refs=array_column($db->getList('explain '.$sql),'ref');
            $base_expr=null;
            foreach($refs as $ref) {
              if (!empty($ref)){
                  $base_expr=$ref;
                  break;
              }
            }
            if (empty($base_expr)) $base_expr=$key['base_expr'];
            if (contain($base_expr,'.*')) $base_expr='*';
            $key=array('expr_type'=>'aggregate_function',
                       'base_expr'=>'count',
                       'sub_tree'=> array(array('expr_type'=>'colref','base_expr'=>$base_expr, 'sub_tree'=>$key['sub_tree'])),
                       'alias'=>array('as'=>true,'name'=>$alias,'base_expr'=>'as '.$alias,'no_quotes'=>$alias));
          }
          $key['delim'] = false;

          $parse['SELECT']=array($key);
          $newSql=new PHPSQLCreator($parse);
          $sql=$newSql->created;
      } 
      */
    $sql=str_replace("\n"," ",$sql);
    while(1){
        if (strpos($sql,'  ')===false) break;
        $sql=trim(str_replace("  "," ",$sql));
    }  
    $sql=trim($sql);
    $re = '/select(.+?)from(.+)/mi'; 
    if (preg_match($re, trim($sql), $matches)){
      $sql='select count(*) as `count` from '.$matches[2];
      $db=new db_query(null,$database,$user,$pwd,$host); 
      $db->query($sql);
      return $db->getString(0);        
    } else {
        throw new Exception('Erreur analyse sql de: '.$sql,__FILE__,__LINE__,null,99);
        exit;
    }
  }
  function db_trans($database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_perform(null,null,null,null,$database,$user,$pwd,$host);
    return $db;    
  }
  function db_perform($table,$data,$action='insert',$parameters='',$database=null, $user=null, $pwd=null, $host='localhost'){
    if (empty($action)) $action='insert';  
    if (empty($parameters)) $parameters='';  
    $db=new db_perform($table,$data,$action,$parameters,$database,$user,$pwd,$host);
    return $db;
  }
  function db_database_exist($database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->database_exist($database);   
  }
  function db_table_exist($tables,$database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure('mysql',$user,$pwd,$host); 
    $list=array_column($db->getList('show databases'),'Database');
    if (!in_array($database,$list)) return false;
    return $db->table_exist($tables,$database);   
  }
  function db_tables_inUse($database=_DB_NAME_, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure('mysql',$user,$pwd,$host); 
    return $db->tables_inUse($database);   
  }
  function db_getPartitions($tables,$database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->getPartitions($tables,$database);   
  }
  function db_getTables($database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->getTables($database);   
  }
  function db_getfields($table, $database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->getfields($table, $database);   
  }
  function db_field_exists($table, $field, $database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->field_exists($table, $field);   
  }
  function db_getPrimaryKey($table, $database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->getPrimaryKey($table,$database);   
  }
  function db_enumGet($table, $field, $database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->enumGet($table, $field);   
  }
 function db_enumCheck($table, $field, $values, $database=null, $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->enumCheck($table, $field, $values);   
  }
  function db_getFieldsStructure($table, $database='', $field='', $user=null, $pwd=null, $host='localhost') {
    $db=new db_structure($database,$user,$pwd,$host); 
    return $db->getFieldsStructure($table, $database, $field);   
  }
  function db_getIndex($table,$name=null,$db=null){
    $sql = 'show index from '.$table;
    $result=db_getList($sql,null,null,null,$db);

    foreach ($result as $r) {
    $new[$r['Key_name']] = $r;    
    }
    $result = $new;
    if (!empty($name)) {
        if (array_key_exists($name, $result)) {
            $result=$result[$name];
        }
        else $result=false;
    }        

    return $result;
  }  
  
  
/**
* Fait un "insert into tbl (select ***)
* 
* Ex: select into orders_stats_1 select (orders_id, year(orders_date), count(*))
* devient mapper('orders_stats_1',
*                array('orders_id'=>'orders_id','year'=>'year(orders_date)'),'compte'=>count(*)),
*                'from orders')
* 
* @param mixed $insertOrTable: La table ou insÃ©rer ou le dÃ©but de la requette
*                            Ex: orders_stats_1 
*                            Ex insert ignore into 
* @param mixed $dataMap: Le contenu du select
*                        un array de pair (champDest=>champSource).
*                        L'ordre des pairs ne compte pas
* @param mixed $condition le reste de la requette de select
*/  
  function db_mapper($insertOrTable,$dataMap,$fromWhere='',$order=null,$database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_structure($database,$user,$pwd,$host);       
    return $db->mapper($insertOrTable,$dataMap,$fromWhere,$order,true);
  }
  function db_mapper_SQL($insertOrTable,$dataMap,$fromWhere='',$order=null,$database=null, $user=null, $pwd=null, $host='localhost'){
    $db=new db_structure($database,$user,$pwd,$host);       
    return $db->mapper($insertOrTable,$dataMap,$fromWhere,$order,false);
  }  
/**
* Supprime les tables temporaires plus anciennes que 1 heure
*   
*/
  function db_clean_tmp_tables(){
      $db=new db_structure();
      $sql="SELECT IF (`UPDATE_TIME` is not null,`UPDATE_TIME`,`CREATE_TIME`) as at,`TABLE_NAME`,`table_schema` FROM INFORMATION_SCHEMA.TABLES
  WHERE table_name like 'tmp%' ";
      $list=$db->getList($sql);
      if (sizeof($list)>0){
          foreach($list as $l){
              if (strtotime($l['at'].' +1 hours')<strtotime('now')){
                  $sql='drop table `'.$l['table_schema'].'`.`'.$l['TABLE_NAME'].'`';
                  $db->query($sql);
              }
          }
      }
     
  }
  /* routines de maintenance */
  if (rand(0,1000)<5) db_clean_tmp_tables();
} 

?>
